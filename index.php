<?php
	/**
	 * Example site, complete with mini backend framework - by Raice Hannay
	 * If you're reading this, you have been granted the privilege of browsing my source code!
	 */

	session_start();

	if (strpos($_SERVER["DOCUMENT_ROOT"], "home3") !== false) {
		// Live
		define("ROOT", "/home3/voodoocr/example/public_html");
	} else {
		// Dev
		define("ROOT", "E:/media/websites/Example");
	}

	require_once(ROOT . "/system/core/functions.php");

	$LOADER		= InitClass("loader", "core/engines");
	$CONFIG		= InitClass("config", "core/engines");
	$DATABASE	= InitClass("database", "core/engines");
	$ROUTER		= InitClass("router", "core/engines");
	$RENDERER	= InitClass("renderer", "core/engines");

	Load(ROOT . "/system/core/installer.php");
	Load(ROOT . "/system/core/model.php");
	Load(ROOT . "/system/core/controller.php");

	$ROUTER->Route();

	echo Render()->Page();

	$DATABASE->Close();