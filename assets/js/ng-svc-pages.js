var ngPagesService = angular.module('ngPagesService', ['ngResource'])
    .factory('Page', function ($resource) {
        'use strict';

        return $resource('/api/pages/get/');
    });