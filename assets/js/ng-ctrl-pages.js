var ngPages = angular.module('ngPages', ['ngPagesService']);

ngPages.controller('PagesCtrl', function ($scope, $http, $window, Page) {
    'use strict';

    $scope.pages = Page.query();

    $scope.New = function () {
        $scope.currentPage = {
            'key':          '',
            'title':        '',
            'slug':         '',
            'class':        '',
            'description':  '',
            'content':      ''
        };
    }

    $scope.New();

    $scope.Edit = function (pageId) {
        $.each($scope.pages, function (key, val) {
            if (val.id == pageId) {
                $scope.currentPage = val;
                $scope.currentPage.key = key;
                return false;
            }
        });
    };

    $scope.Save = function () {
        var savePath = '/api/page/save/';

        $http.post(savePath, $scope.currentPage)
            .success(function (data) {
                $scope.pages = Page.query();
            });
    }
});
