<?php
	class Model {
		#region Declarations

		protected $db;
		protected $table;
		protected $type			= 'single';
		protected $assocMulti	= array();
		protected $assocSingle	= array();
		protected $validation	= array();
		protected $limit		= null;
		protected $count		= 0;
		protected $hasResults	= false;

		#endregion Declarations

		public function __construct() {
			if (empty($this->db))
				$this->db = Database();

			$this->db->From($this->table);
		}

		#region Public

		#region Data manipulation

		public function Select($column) {
			$this->db->Select($column);

			return $this;
		}

		public function Is($type) {
			$type = strtolower($type);

			$this->type = $type;

			return $this;
		}

		public function Where($column, $value) {
			$this->db->Where($column, $value);

			if ($column == 'id' && empty($limit))
				$this->limit = 1;
			else
				$this->limit = null;

			return $this;
		}

		public function WhereNot($column, $value) {
			$this->db->WhereNot($column, $value);

			return $this;
		}

		public function WhereLike($column, $value) {
			$this->db->WhereLike($column, $value);

			return $this;
		}

		public function WhereBeforeLike($column, $value) {
			$this->db->WhereBeforeLike($column, $value);

			return $this;
		}

		public function WhereAfterLike($column, $value) {
			$this->db->WhereAfterLike($column, $value);

			return $this;
		}

		public function WhereIn($column, $values) {
			$this->db->WhereIn($column, $values);

			return $this;
		}

		public function WhereNotIn($column, $values) {
			$this->db->WhereNotIn($column, $values);

			return $this;
		}

		public function WhereInSet($column, $values) {
			$this->db->WhereInSet($column, $values);

			return $this;
		}

		public function Orr() {
			$this->db->Orr();

			return $this;
		}

		public function GroupAnd() {
			$this->db->GroupAnd();

			return $this;
		}

		public function GroupOr() {
			$this->db->GroupOr();

			return $this;
		}

		public function Order($column, $direction) {
			$this->db->Order($column, $direction);

			return $this;
		}

		public function Limit($value) {
			$this->db->Limit($value);
			$this->limit = $value;

			return $this;
		}

		public function Get() {
			$this->__construct();

			// Set joins
			if (!empty($this->join)) {
				foreach ($this->join as $key => $val) {
					$key = explode('.', $key);
					$val = explode('.', $val);

					$this->db->LeftJoin($key[0])->On($val[1], $key[1]);
				}
			}

			// Execute database query
			$result = $this->db->Get('object');

			if (!empty($result))
				$this->count = count($result);

			$this->hasResults = true;

			// Set model properties
			if (is_array($result))
				$this->Is('multi');

			if ($this->limit == 1)
				$this->Is('single');
			else
				$this->Is('multi');

			if ($this->type != 'multi') {
				foreach ($result as $key => $val)
					$this->{$key} = $val;
			}else{
				if ($this->count < 2)
					$result = array($result);

				foreach ($result as $key => $val)
					if (!empty($val))
						$this->{$key} = Load()->Model( $this->ClassName() )->Where('id', $val->id)->Get();
			}

			// Add extensions to base model
			if (!empty($result) && $this->type != 'multi') {
				// Set 'multi' associations
				if (!empty($this->assocMulti)) {
					foreach ($this->assocMulti as $key => $val) {
						$key				= strToCamel($key);
						$modelAndProperty	= explode('.', $val[0]);

						$model		= $modelAndProperty[0];
						$property	= $modelAndProperty[1];

						$on = explode('.', $val[1]);
						$on = isset($on[1]) ? $on[1] : $on[0];

						if (isset($this->{$on}))
							$this->$key = Load()
								->Model($model)
								->Is('multi')
								->Where($property, $this->{$on});
					}
				}
			}

			// Set 'single' associations
			if (!empty($this->assocSingle)) {
				foreach ($this->assocSingle as $key => $val) {
					$key    = strToCamel($key);
					$modelAndProperty	= explode('.', $val[0]);

					$model		= $modelAndProperty[0];
					$property	= $modelAndProperty[1];

					$on = explode('.', $val[1]);
					$on = isset($on[1]) ? $on[1] : $on[0];

					if ($this->type == 'single')
						$this->{$key} = Load()
							->Model($model)
							->Is('multi')
							->Where($property, $this->{$on});
					else
						foreach ($this as $propKey => $propVal)
							if (is_numeric($propKey) && is_object($propVal))
								$this->{$propKey}->{$key} = Load()
									->Model($model)
									->Is('multi')
									->Where($property, $this->{$propKey}->{$on});
				}
			}

			return $this;
		}

		public function Count() {
			if (!$this->hasResults)
				$this->Get();

			return $this->count;
		}

		public function Save() {
			$this->db->Table($this->table);

			$columnsSet = 0;

			foreach ($this->validation as $key => $val) {
				if (isset($this->{$key})) {
					$this->db->Set($key, $this->{$key});

					$columnsSet++;
				}

				if ($val['type'] == 'timeAdded' && empty($this->{$key}))
					$this->db->Set($key, now());

				if ($val['type'] == 'timeUpdated')
					$this->db->Set($key, now());
			}

			if (!empty($this->id))
				$this->db->Where('id', $this->id);

			if ($columnsSet > 0)
				$id = $this->db->Execute();
			else
				trigger_error("No columns were set, may have been caused by loading the wrong model", E_USER_ERROR);

			$this->Where('id', $id)->Get();

			return true;
		}

		#endregion Data manipulation

		/**
		 * Returns name of class, excluding 'Model' suffix
		 *
		 * @return string Name of class
		 *
		 */
		public function ClassName() {
			$className = get_called_class();

			return $className;
		}

		public function Validation($fieldName = null) {
			if (!empty($fieldName)) {
				$validation                     = $this->validation[$fieldName];
				$validation['name']     = $fieldName;

				return $validation;
			}else{
				return $this->validation;
			}
		}

		public function PostUpdate() {
			foreach ($this->validation as $key => $val) {
				if (isset($_POST[$key])) {
					$this->{$key} = $_POST[$key];
				}
			}

			return $this;
		}

		public function Validate($fieldName = null) {
			$errors = array();

			if (empty($fieldName)) {
				foreach ($this->validation as $key => $val) {
					if (!empty($this->{$key})) {
						$validationMethod = "Validate" . firsttoupper($val['type']);

						if (method_exists(Input(), $validationMethod)) {
							$response = Input()->{$validationMethod}($this->{$key});

							if (!is_bool($response)) {
								$this->validation[$key]['error'] = $response;

								$errors[$key] = $response;
							}
						}
					}else{
						if (!empty($val['required'])) {
							$this->validation[$key]['error'] = Language()->Validation->empty;

							$errors[$key] = Language()->Validation->empty;
						}
					}
				}

				if (!empty($errors))
					return false;
			}

			return true;
		}
		#endregion
	}