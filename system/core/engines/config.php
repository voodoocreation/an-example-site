<?php
	class Config {
		private $config = array();

		public function __construct() {
			$configJSON = Load()->Buffer(ROOT . "/system/config/config.json");

			$this->config = json_decode($configJSON);
		}

		public function __get($key) {
			if (!isset($this->$key)) {
				return $this->config->$key;
			}

			return $this->$key;
		}
	}