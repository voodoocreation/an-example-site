<?php
	class Router {
		#region Declarations

		private $uri;
		private $route;
		private $routes		= array();
		private $isRouted	= false;

		public $controller;
		public $controllerName;
		public $controllerPath;
		public $action;
		public $segments	= array();

		#endregion Declarations

		public function __construct() {
			$this->RepairGET();
			$this->DefinePaths();
			$this->DefineRouting();
		}

		#region Private

		/**
		 * Define path segments
		 */
		private function DefinePaths() {
			// Define URI
			$this->uri = explode("?", $_SERVER["REQUEST_URI"]);
			$this->uri = $this->uri[0];

			if (substr($this->uri, -1, 1) !== "/") {
				// Append trailing slash for consistency
				$this->uri = $this->uri . "/";
			}

			// Define URI segments array
			$this->segments = explode("/", substr($this->uri, 1, -1));
		}

		/**
		 * Define routing rules
		 */
		private function DefineRouting() {
			$routesJSON = Load()->Buffer(ROOT . "/system/config/routes.json");
			$routes = json_decode($routesJSON);

			// Initial routing check to define controller
			$route = $this->RouteExists($routes);

			if (!empty($route)) {
				// Base routing has been defined
				$controller = explode('/', $route);
				$controller = $controller[0];
			}else{
				// No base routing defined, use default /controller/method/params/ method
				$controller = $this->segments[0];
			}

			// Compile routing array
			foreach ($routes as $uri => $controllerAction) {
				if (substr($uri, -1, 1) !== "/") {
					$uri = $uri . "/";
				}

				if (strpos($controllerAction, '/') === false) {
					$controllerAction = strToCamel($controller) . "/" . $controllerAction;
				}

				$this->routes[$uri] = $controllerAction;
			}

			$route = $this->RouteExists();

			if (!empty($route)) {
				// Use defined routing
				$routeArray = explode('/', $route);

				$this->route			= $route;
				$this->controllerName	= $routeArray[0];
				$this->controllerPath	= ROOT . "/system/controllers/" . strToPath($this->controllerName) . ".php";
				$this->action			= $routeArray[1];
			} else {
				// Use default /controller/method/params/ method if exists
				$controllerPath = ROOT . "/system/controllers/" . strToPath($controller) . ".php";
				if (file_exists($controllerPath)) {
					$this->route			= strToPath($this->controllerName) . '/' . strToPath($this->action);
					$this->controllerName	= strToCamel($controller);
					$this->controllerPath	= ROOT . "/system/controllers/" . strToPath($controller) . ".php";
					$this->action			= isset($this->segments[1]) ? strToCamel($this->segments[1]) : 'Index';
				} else {
					$this->route			= "Frontend/Post";
					$this->controllerName	= "Frontend";
					$this->controllerPath	= ROOT . "/system/controllers/frontend.php";
					$this->action			= "Post";
				}
			}
		}

		/**
		 * Repair $_GET superglobal after mod_rewrite behaviour
		 */
		private function RepairGET() {
			$_GET = array();

			$uri = $_SERVER['REQUEST_URI'];

			if (strpos($uri, '?') !== false) {
				$query = substr($uri, strpos($uri, '?') + 1);

				// Loop through all GET params and set them in $_GET
				foreach (explode('&', $query) as $part) {
					$part = urldecode($part);

					if (strpos($part, '=') !== false) {
						$pair[0] = substr($part, 0, strpos($part, '='));
						$pair[1] = substr($part, 0, strpos($part, '=') + 1);
					}else{
						$pair[0] = $part;
					}

					if (strpos($pair[0], '[') !== false) {
						// Param is an array
						$key1 = substr($pair[0], 0, strpos($pair[0], '['));
						$key2 = substr($pair[0], strpos($pair[0], '['), strrpos($pair[0], ']'));

						if ($key2 != '[]')
							$key2 = str_replace(array('[', ']'), array('["', '"]'), $key2);

						eval("\$_GET['$key1']$key2 = $pair[1];");
					}else{
						// Param is a string
						$_GET[$pair[0]] = isset($pair[1]) ? $pair[1] : null;
					}
				}
			}

		}

		/**
		 * Check if route exists and return valid route
		 *
		 * @param object|array $routes Routing rules
		 *
		 * @return string Controller/Action
		 */
		private function RouteExists($routes = null) {
			$finalRoute = false;

			if (empty($routes)) {
				$routes = $this->routes;
			}

			foreach ($routes as $uri => $controllerAction) {
				if (($uri !== "/" && strpos(strtolower($this->uri), $uri) === 0) || ($this->uri === $uri)) {
					$finalRoute = $controllerAction;
				}
			}

			return $finalRoute;
		}

		#endregion Private

		#region Public

		/**
		 * Reroute to a specified controller action
		 * @param string $action Controller action to route to
		 * @param string $controller Controller name if not routing to current controller
		 */
		public function ReRoute($action, $controller = null) {
			if (!empty($controller)) {
				$controller = strToPath($controller);

				$this->controllerName	= strToCamel($controller);
				$this->controllerPath	= ROOT . "/system/controllers/$controller";
			}

			$this->action = strToCamel($action);

			if ($this->isRouted) {
				$this->Route();
			}
		}

		/**
		 * Perform route
		 *
		 * @return bool
		 */
		public function Route() {
			if (file_exists($this->controllerPath)) {
				Load($this->controllerPath);

				$className = $this->controllerName . "Controller";

				$this->controller = new $className();

				if (method_exists($this->controller, $this->action)) {
					// Define action parameters
					$index = 2;
					foreach ($this->segments as $key => $val) {
						if (strToCamel($val) === $this->action) {
							$index = $key + 1;
						}
					}
					$params = array_slice($this->segments, $index);

					// Call action
					call_user_func_array(array(
						$this->controller,
						$this->action
						), $params);

					$this->isRouted = true;

					return true;
				}
			}

			HTTP("404");
		}

		#endregion Public
	}