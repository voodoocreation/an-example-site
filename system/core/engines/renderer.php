<?php
	class Renderer {
		#region Declarations

		private $template;
		private $tokens = array(
			"alert" => null,
			"scripts" => null
			);

		#endregion Declarations

		#region Public

		/**
		 * Render a controller action
		 *
		 * @param string $controllerName Name of controller
		 * @param string $action Name of action to call
		 * @param array $params Array of parameters to pass to the action
		 *
		 * @return string | bool if controller action does not exist
		 */
		public function Action($controllerName, $action, $params = array()) {
			$controllerName	= strToCamel($controllerName);
			$action			= strToCamel($action);

			$output = null;
			$return	= null;

			$preExistingTokens = $this->tokens;

			if (Route()->controllerName === $controllerName) {
				// Action is on current controller
				$controller = Route()->controller;
			} else {
				// Action on different controller, load the controller
				$controllerPath = ROOT . "/system/controllers/" . strToPath($controllerName) . ".php";

				if (file_exists($controllerPath)) {
					// Controller file exists
					Load($controllerPath);

					$controllerName .= "Controller";
					$controller		 = new $controllerName();
				} else {
					trigger_error("Controller does not exist at '" . $controllerPath . "'", E_USER_ERROR);
					return false;
				}
			}

			if (method_exists($controller, $action)) {
				// Controller action exists, get output from it
				ob_start();

				call_user_func_array(array(
					$controller,
					$action
					), $params);

				$output = ob_get_clean();
			} else {
				trigger_error("Action '" . $action . "' does not exist on controller '" . $controllerName . "'", E_USER_ERROR);
				return false;
			}

			// Get the new tokens by comparing the pre-existing tokens with the ones that exist after the action call
			$newTokens = array_diff($this->tokens, $preExistingTokens);

			// Define HTML to return
			if (!empty($newTokens)) {
				// New tokens were loaded - build return string from them
				foreach ($newTokens as $val) {
					$return .= $val;
				}
			} else {
				// No new tokens were loaded - define return string using the output of the action
				$return = $output;
			}

			$this->tokens = $preExistingTokens;

			return $return;
		}

		/**
		 * Display alert message
		 *
		 * @param string $message Message to display
		 * @param string $class Alert message class
		 */
		public function Alert($message, $class = "info") {
			$class = "alert-" . $class;

			$html	= '<div class="alert ' . $class . '">' . $message . '</div>' . PHP_EOL;

			$this->tokens["alert"] .= $html;
		}

		/**
		 * Encode data as JSON and output
		 *
		 * @param object|array $toEncode Data to encode and output
		 */
		public function JSON($toEncode) {
			$result = json_encode($toEncode);

			echo $result;
			exit();
		}

		/**
		 * Render page
		 *
		 * @return string Rendered page HTML
		 */
		public function Page() {
			if (empty($this->template)) {
				trigger_error("Rendering template has not been set", E_USER_ERROR);
				return false;
			}

			$template = Load()->Buffer($this->template);

			// Replace template action tokens with values
			preg_match_all('/\[\[(.*?)\]\]/', $template, $actions);
			foreach ($actions[0] as $action) {
				$actionString = str_replace(array("[", "]"), "", $action);

				// Define controller
				$split		= explode(".", $actionString);
				$controller	= $split[0];

				// Define action
				$split	= explode("(", $split[1]);
				$action	= $split[0];

				// Define action params if they exist
				$params = array();
				if (isset($split[1])) {
					// Remove closing bracket
					$split[1] = substr($split[1], 0, -1);

					// Separate params
					$split = explode(",", $split[1]);

					// Define params
					foreach ($split as $param) {
						$param = trim($param);

						// Remove quote marks, if present
						if (strpos($param, "'") === 0 || strpos($param, '"') === 0) {
							$param = substr($param, 1, -1);
						}

						$params[] = $param;
					}
				}

				// Exec controller action and replace action token with value
				$actionToken = $this->Action($controller, $action, $params);
				$template = str_replace("[[" . $actionString . "]]", $actionToken, $template);
			}

			// Replace template tokens with token values
			preg_match_all('/{{(.*?)}}/', $template, $tokens);
			foreach ($tokens[0] as $token) {
				$token = str_replace(array("{", "}"), "", $token);

				$renderedToken = isset($this->tokens[$token]) ? $this->tokens[$token] : "<!-- " . $token . " -->";

				$template = str_replace("{{" . $token . "}}", $renderedToken, $template);
			}

			return $template;
		}

		/**
		 * Define current template
		 *
		 * @param string $templateName Template filename
		 */
		public function Template($templateName) {
			$templateName = strToPath($templateName);
			$fileName = str_replace(".template", "", $templateName) . ".template";
			$filePath = ROOT . "/templates/" . $fileName;

			if (file_exists($filePath)) {
				$this->template = $filePath;
			} else {
				trigger_error("Template does not exist at '" . $filePath . "'", E_USER_ERROR);
			}
		}

		/**
		 * Add token to renderer for use in page template output
		 *
		 * @param string $key Token name
		 * @param string $val Token value
		 *
		 * @return bool
		 */
		public function Token($key, $val) {
			$this->tokens[$key] = $val;

			return true;
		}

		/**
		 * Append data to the end of an existing token
		 *
		 * @param string $key Token name
		 * @param string $val Token value
		 *
		 * @return bool
		 */
		public function TokenAppend($key, $val) {
			if (isset($this->tokens[$key])) {
				$this->tokens[$key] .= $val;
			} else {
				$this->tokens[$key] = $val;
			}

			return true;
		}

		#endregion Public
	}