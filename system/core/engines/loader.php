<?php
	class Loader {
		#region Public

		/**
		 * Load include into buffer and return output
		 *
		 * @param string $path Path to include
		 *
		 * @return string Buffered output
		 */
		public function Buffer($path) {
			ob_start();
			$this->Inc($path);
			$return = ob_get_clean();

			return $return;
		}

		/**
		 * Include file
		 *
		 * @param string $path Path to include
		 */
		public function Inc($path) {
			require($path);
		}

		/**
		 * Add JavaScript file to template
		 *
		 * @param string $script Script filename, relative to /assets/js/
		 */
		public function JS($script) {
			$html = '  <script src="/assets/js/' . $script . '"></script>' . PHP_EOL;

			Render()->TokenAppend("scripts", $html);
		}

		/**
		 * Load a package class from the library
		 *
		 * @param string $package Package name
		 *
		 * @return bool
		 */
		public function Library($package) {
			$packageFile = strToPath($package);

			$path = ROOT . "/system/library/" . $packageFile . "/" . $packageFile . ".php";
			if (file_exists($path) && !class_exists($package)) {
				require_once($path);
				return true;
			}

			trigger_error("Library package does not exist at '" . $path . "'", E_USER_ERROR);
			return false;
		}

		/**
		 * Load model
		 *
		 * @param string $modelName Model name
		 *
		 * @return Model | boolean if model does not exist
		 */
		public function Model($modelName) {
			$modelName = strToCamel($modelName);
			$modelPath = strToPath($modelName);
			$modelPath = ROOT . "/system/models/" . $modelPath . ".php";

			if (file_exists($modelPath)) {
				// Model path is valid
				if (class_exists($modelName) === false) {
					require_once($modelPath);
				}

				$model = new $modelName();
			} else {
				trigger_error("Model does not exist at '" . $modelPath . "'", E_USER_ERROR);
				return false;
			}

			return $model;
		}

		/**
		 * Load view into token list
		 * @param string		$viewName		Name of view to load
		 * @param array			$data			Array of data to convert to local view vars
		 * @param string|null	$controllerName	Name of controller the view belongs to
		 * @param string		$tokenName		The token key to store view against
		 *
		 * @return bool
		 */
		public function View($viewName, $data = array(), $controllerName = null, $tokenName = "view") {
			// Define view data
			if (is_array($data)) {
				foreach ($data as $key => $val) {
					${$key} = $val;
				}
			}

			// Define view path
			$viewPath = ROOT . "/system/views/";
			if (empty($controllerName)) {
				// Get view for current controller
				$viewPath .= strToPath(Route()->controllerName);
			} else {
				$viewPath .= strToPath($controllerName);
			}
			$viewPath .= "/" . strToPath($viewName) . ".php";

			// Load the view
			if (file_exists($viewPath)) {
				ob_start();
				require_once($viewPath);
				$view = ob_get_clean();

				Render()->Token($tokenName, $view);
			} else {
				trigger_error("View does not exist at '" . $viewPath . "'", E_USER_ERROR);
				return false;
			}

			return true;
		}

		#endregion Public
	}