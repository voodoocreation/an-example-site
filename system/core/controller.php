<?php
	Load()->Library("Aes");

	/**
	 * Base controller
	 * Contains all base controller functionality required by child controllers
	 */
	class Controller {
		#region Declarations

		private $Installer;

		public $name = "Base Controller";
		public $user;

		#endregion

		#region Construct
		public function __construct() {
			$this->Install();

			Render()->Template("default");
		}
		#endregion

		#region Private

		private function Install() {
			$installerPath = ROOT . "/system/installers/" . strToPath(Route()->controllerName) . "-installer.php";

			if (file_exists($installerPath)) {
				require_once($installerPath);

				$this->Installer = new Install($this->name);
			}
		}

		/**
		 * Check if user is logged in, if so, define current user
		 * @return bool
		 */
		protected function LoginCheck() {
			if (isset($_SESSION["id"])) {
				$userId = $this->Decrypt($_SESSION["id"], session_id());

				$user = Load()->Model("user")
					->Where("id", $userId)
					->Limit(1)
					->Get();

				if ($user->Count() > 0) {
					$this->user = $user;

					return true;
				}
			}

			Route("Login");

			return false;
		}
		#endregion

		#region Public

		/**
		 * 3-level 256 bit encryption method
		 *
		 * @param string|array|object $toEncrypt Data to encrypt
		 * @param string $password Password to encrypt with
		 * @return string Encrypted data
		 */
		public function Encrypt($toEncrypt, $password) {
			$salt = Config("encrypt_salt");

			if (!is_string($toEncrypt)) {
				$toEncrypt = 'serial' . serialize($toEncrypt);
			}

			$encryptLv1 = strrev(base64_encode($toEncrypt));

			if (strlen($encryptLv1) > 2) {
				$split = str_split($encryptLv1, (strlen($encryptLv1) / 2) + 1);
			} elseif (strlen($encryptLv1) > 1) {
				$split = str_split($encryptLv1, (strlen($encryptLv1) / 2));
			} else {
				$split = array($encryptLv1, "");
			}

			$encryptLv2 = $split[0] . $salt . $split[1];
			$encryptLv3 = AesCtr::encrypt($encryptLv2, $password, 256);

			return $encryptLv3;
		}

		/**
		 * 3-level 256 bit decryption method
		 *
		 * @param string $toDecrypt Data to decrypt
		 * @param string $password Password to decrypt with
		 * @return string Decrypted data
		 */
		public function Decrypt($toDecrypt, $password) {
			$salt = Config("encrypt_salt");
			$encryptLv3 = AesCtr::decrypt($toDecrypt, $password, 256);
			$encryptLv2 = str_replace($salt, null, $encryptLv3);
			$encryptLv1 = base64_decode(strrev($encryptLv2) );

			if (strpos($encryptLv1, 'serial') !== false) {
				$encryptLv1 = unserialize(str_replace('serial', null, $encryptLv1));
			}

			return $encryptLv1;
		}

		/**
		 * Secure hasher
		 *
		 * @param string $toHash String to hash
		 * @param string $salt String to use as salt
		 * @return string Hashed string
		 */
		public function Hash($toHash, $salt) {
			$hashSalt = Config('hash_salt');

			if (strlen($toHash) > 2) {
				$split = str_split($toHash, (strlen($toHash) / 2) + 1);
			} elseif (strlen($toHash) > 1) {
				$split = str_split($toHash, (strlen($toHash) / 2));
			} else {
				$split = array($toHash, 'v');
			}

			$hashed = hash('sha512', $salt . $split[0] . $hashSalt . $split[1]);

			return $hashed;
		}

		/**
		 * Set title of current page/view
		 *
		 * @param string $title Title string to set
		 * @return void
		 *
		 */
		public function Title($title) {
			$title = trim($title);

			Render()->Token('title', $title);
		}

		#region Actions

		public function Login() {
			$this->Title("Log in");
			Render()->Token("class", "enter");

			if (isPosted()) {
				$user = Load()->Model("user")
					->Where("username", $_POST["username"])
					->Where("password", $this->Hash($_POST["password"], $_POST["username"]))
					->Limit(1)
					->Get();

				if ($user->Count() > 0) {
					$_SESSION["id"] = $this->Encrypt($user->id, session_id());
					redirect();
				} else {
					Render()->Alert("Your login details were incorrect.", "danger");
				}
			}

			Load()->View("login");
		}

		public function Logout() {
			$this->Title("Log out");
			Render()->Token("class", "exit");

			$this->user = null;
			session_destroy();

			Render()->Alert("You have been logged out.", "success");

			Load()->View("login");
		}

		#endregion Actions

		#endregion
	}