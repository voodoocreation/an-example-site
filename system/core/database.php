<?php
	class Database {
		#region Properties
		private $db;
		private $dbName;
		private $dbHost;
		private $dbUser;
		private $dbPass;

		private $select	= array();
		private $from	= null;
		private $join	= array();
		private $where	= array();
		private $group	= null;
		private $order	= array();
		private $limit	= null;

		private $set	= array();

		private static $cache = array();
		#endregion

		#region Manipulation
		private $table		= array();
		private $columns	= array();
		#endregion

		#region Construct
		public function __construct() {
			$this->dbName = Config('db_name');
			$this->dbHost = Config('db_host');
			$this->dbUser = Config('db_user');
			$this->dbPass = Config('db_password');

			$this->Connect();
		}
		#endregion

		#region Clone
		public function __clone() {
			$this->Reset();
		}
		#endregion

		#region Private
		#region Helpers
		/**
		 * Connect to database server and select the database
		 *
		 * @return void
		 *
		 */
		private function Connect() {
			$this->db = mysqli_connect($this->dbHost, $this->dbUser, $this->dbPass)
			or die('Incorrect database account details');

			$this->db->select_db($this->dbName);
		}

		/**
		 * Generate string of all column properties to be used in a SQL query
		 *
		 * @param array $column Column parameter array in Creator format
		 * @return string Column properties SQL
		 *
		 */
		private function GenerateColumnSQL($column) {
			// Use 'rename' instead of name
			if (!empty($column['rename']))
				$column['name'] = $column['rename'];

			// Define column name and type
			$sql  = "`" . $column['name'] . "` ";
			$sql .= $column['type'] . (!empty($column['length']) ? "(" . $column['length'] . ")" : null) . " ";

			// Define collation
			if (!empty($column['collation'])) {
				$charSet = substr($column['collation'], 0, strpos($column['collation'], '_'));

				$sql .= "CHARACTER SET $charSet COLLATE " . $column['collation'] . " ";
			}

			// Define attributes
			if (!empty($column['attributes']))
				$sql .= $column['attributes'] . " ";

			// Define NULL status
			if ($column['null'])
				$sql .= "NULL ";
			else
				$sql .= "NOT NULL ";

			// Define DEFAULT value
			if (strlen($column['default']) > 0) {
				if (strtoupper($column['default']) == 'NULL')
					$sql .= "DEFAULT NULL ";
				elseif (strpos($column['default'], ' ') === false)
					$sql .= "DEFAULT " . $column['default'] . " ";
				else
					$sql .= "DEFAULT '" . $column['default'] . "' ";
			}

			// Define extra
			if ($column['extra'])
				$sql .= $column['extra'] . " ";

			// Define column comments
			if (!empty($column['comments']))
				$sql .= "COMMENT '" . $column['comments'] . "' ";

			$sql = substr($sql, 0, -1);

			return $sql;
		}

		/**
		 * Generate array of SQL for indexes and foreign key constraints for specified column
		 *
		 * @param array $column Column parameter array in Creator format
		 * @return array Array of SQL for column indexes
		 *
		 */
		private function GenerateColumnIndexes($column) {
			$indexes = array();

			if (!empty($column['rename']))
				$column['name'] = $column['rename'];

			// Define indexes
			if (!empty($column['index'])) {
				switch($column['index']) {
					case 'PRIMARY':
						$indexes[] = "PRIMARY KEY (`" . $column['name'] . "`)";
						break;

					case 'UNIQUE':
						$indexes[] = "UNIQUE KEY `" . $column['name'] . "` (`" .  $column['name'] . "`)";
						break;

					case 'FULLTEXT':
						$indexes[] = "FULLTEXT KEY `" . $column['name'] . "` (`" .  $column['name'] . "`)";
						break;

					default:
						$indexes[] = "KEY `" . $column['name'] . "` (`" .  $column['name'] . "`)";
						break;
				}
			}

			// Define foreign keys
			if (!empty($column['foreign'])) {
				$indexSearch	= "KEY `" . $column['name'] . "` (`" .  $column['name'] . "`)";
				$indexReplace	= "KEY `fk_" . $this->table['name'] . '_' . $column['name'] . "` (`" .  $column['name'] . "`)";

				$key = array_search($indexSearch, $indexes);

				if (!is_bool($key))
					$indexes[$key] = $indexReplace;
				else
					$indexes[] = $indexReplace;

				$refTable	= substr($column['foreign']['references'], 0, strpos($column['foreign']['references'], '.'));
				$refColumn	= substr($column['foreign']['references'], strpos($column['foreign']['references'], '.') + 1);

				#region Define ON DELETE action
				$column['foreign']['delete'] = strtoupper($column['foreign']['delete']);

				switch($column['foreign']['delete']) {
					case 'CASCADE':
					case 'RESTRICT':
					case 'SET NULL':
					case 'NO ACTION':
						$actionDelete = $column['foreign']['delete'];
						break;

					case 'NULL':
						$actionDelete = 'SET NULL';
						break;

					case 'NONE':
					default:
						$actionDelete = 'NO ACTION';
						break;
				}
				#endregion

				#region Define ON UPDATE action
				$column['foreign']['update'] = strtoupper($column['foreign']['update']);

				switch($column['foreign']['update']) {
					case 'CASCADE':
					case 'RESTRICT':
					case 'SET NULL':
					case 'NO ACTION':
						$actionUpdate = $column['foreign']['update'];
						break;

					case 'NULL':
						$actionUpdate = 'SET NULL';
						break;

					case 'NONE':
					default:
						$actionUpdate = 'NO ACTION';
						break;
				}
				#endregion

				$constraint	 = "CONSTRAINT `fk_" . $this->table['name'] . '_' . $column['name'] . "` ";
				$constraint	.= "FOREIGN KEY (`" . $column['name'] . "`) ";
				$constraint	.= "REFERENCES `" . $refTable . "`(`" . $refColumn . "`) ";
				$constraint	.= "ON DELETE $actionDelete ";
				$constraint	.= "ON UPDATE $actionUpdate";

				$indexes[] = $constraint;
			}

			return $indexes;
		}

		/**
		 * Reset all non-permanent properties
		 *
		 * @return void
		 *
		 */
		private function Reset() {
			// Define properties to exclude from resetting
			$exclude = array('db', 'dbName', 'dbHost', 'dbUser', 'dbPass', 'tableCache', 'queryCache');

			// Get default values
			$Reflection	= new ReflectionClass($this);
			$defaults	= $Reflection->getDefaultProperties();

			// Get current
			$properties	= get_object_vars($this);

			// Loop through properties and either replace with defaults or unset
			foreach ($properties as $key => $val) {
				if (!in_array($key, $exclude)) {
					if (isset($defaults[$key]))
						$this->{$key} = $defaults[$key];
					else
						unset($this->{$key});
				}
			}
		}
		#endregion

		#region Query builders
		/**
		 * Call each sub-builder to build entire SQL query
		 *
		 * @return string Full SQL query
		 *
		 */
		private function BuildQuery() {
			$query  = $this->BuildSelect();
			$query .= $this->BuildFrom();
			$query .= $this->BuildJoin();
			$query .= $this->BuildWhere();
			$query .= $this->BuildGroup();
			$query .= $this->BuildOrder();
			$query .= $this->BuildLimit();

			return $query;
		}

		/**
		 * Build SELECT query section
		 *
		 * @return string SELECT query section
		 *
		 */
		private function BuildSelect() {
			$sql = "SELECT ";

			if (!empty($this->select)) {
				foreach ($this->select as $select) {
					$sql .= "$select, ";
				}

				$sql = substr($sql, 0, -2) . " ";
			}else{
				$sql .= " * ";
			}

			return $sql;
		}

		/**
		 * Build FROM query section
		 *
		 * @return string FROM query section
		 *
		 */
		private function BuildFrom() {
			$sql  = "FROM $this->from ";

			return $sql;
		}

		/**
		 * Build JOIN query section
		 *
		 * @return string JOIN query section
		 *
		 */
		private function BuildJoin() {
			$sql = null;

			foreach ($this->join as $key => $val) {
				$sql .= "$val ";

				$sql = substr($sql, 0, -1) . ") ";
			}

			return $sql;
		}

		/**
		 * Build WHERE query section
		 *
		 * @return string WHERE query section
		 *
		 */
		private function BuildWhere() {
			$clauses = array(
				'OR',
				') OR (',
				') AND ('
			);

			if (!empty($this->where)) {
				$sql = "WHERE (";

				foreach ($this->where as $key => $val) {
					$and = (in_array($val, $clauses) || substr($sql, -2, 1) == '(') ? null : 'AND ';

					if ($key == 0 || substr($sql, -3, 2) == 'OR') {
						$sql .= "$val ";
					}else{
						$sql .= "$and$val ";
					}
				}

				$sql = substr($sql, 0, -1) . ") ";

				return $sql;
			}
		}

		/**
		 * Build GROUP BY query section
		 *
		 * @return string GROUP BY query section
		 *
		 */
		private function BuildGroup() {
			if (!empty($this->group)) {
				$sql = "GROUP BY $this->group ";

				return $sql;
			}
		}

		/**
		 * Build ORDER BY query section
		 *
		 * @return string ORDER BY query section
		 *
		 */
		private function BuildOrder() {
			if (!empty($this->order)) {
				$sql = "ORDER BY ";

				foreach ($this->order as $key => $val) {
					$sql .= ($key == 0) ? "$val " : ", $val ";
				}

				return $sql;
			}
		}

		/**
		 * Build LIMIT query section
		 *
		 * @return string LIMIT query section
		 *
		 */
		private function BuildLimit() {
			if (!empty($this->limit)) {
				$sql = "LIMIT $this->limit";

				return $sql;
			}
		}
		#endregion
		#endregion

		#region Public
		#region Helpers
		public function Close() {
			$this->Reset();

			$this->db->kill($this->db->thread_id);
			$this->db->close();
		}

		/**
		 * Check if specified table exists
		 *
		 * @param string $table Table name
		 * @return bool
		 *
		 */
		public function TableExists($table) {
			$table = esc( str_replace('`', '', $table) );
			$table = explode('.', $table);

			if (isset($table[1]))
				$sql = "SHOW TABLES FROM `" . $table[0] . "` LIKE '" . $table[1] . "'";
			else
				$sql = "SHOW TABLES LIKE '" . $table[0] . "'";

			$rs = $this->db->query($sql);

			return (bool) ($rs->num_rows > 0);
		}

		/**
		 * Retrieve all properties of specified table
		 *
		 * @param string $table Table name
		 * @return array Array of all table properties
		 *
		 */
		public function GetTableProperties($table) {
			$formattedResult = array();

			$table = esc($table);

			$sql = "SELECT * FROM `information_schema`.`TABLES` WHERE `TABLE_NAME`='$table'";

			$rs = $this->db->query($sql);

			if (is_object($rs)) {
				$result = $rs->fetch_assoc();

				$formattedResult = array(
					'name'				=> $result['TABLE_NAME'],
					'engine'			=> $result['ENGINE'],
					'collation'			=> $result['TABLE_COLLATION'],
					'comments'			=> $result['TABLE_COMMENT'],
					'database'			=> $result['TABLE_SCHEMA'],
					'catalog'			=> $result['TABLE_CATALOG'],
					'type'				=> $result['TABLE_TYPE'],
					'version'			=> $result['VERSION'],
					'row_format'		=> $result['ROW_FORMAT'],
					'row_count'			=> $result['TABLE_ROWS'],
					'row_avg_length'	=> $result['AVG_ROW_LENGTH'],
					'data_length'		=> $result['DATA_LENGTH'],
					'data_length_max'	=> $result['MAX_DATA_LENGTH'],
					'data_free'			=> $result['DATA_FREE'],
					'index_length'		=> $result['INDEX_LENGTH'],
					'auto_increment'	=> $result['AUTO_INCREMENT'],
					'time_created'		=> $result['CREATE_TIME'],
					'time_updated'		=> $result['UPDATE_TIME'],
					'time_checked'		=> $result['CHECK_TIME'],
					'auto_increment'	=> $result['AUTO_INCREMENT'],
					'checksum'			=> $result['CHECKSUM'],
					'create_options'	=> $result['CREATE_OPTIONS']
				);
			}else{
				trigger_error($this->db->error, E_USER_ERROR);
			}

			return $formattedResult;
		}

		/**
		 * Retrieve all columns from specified table - in Creator format
		 *
		 * @param string $table Table name
		 * @return array Array of table columns with all properties
		 *
		 */
		public function GetTableColumns($table) {
			$result = array();

			if (!empty(self::$cache[$table]))
				return self::$cache[$table];

			$table = esc($table);

			$sql = "SHOW FULL COLUMNS FROM `$table`";

			$rs = $this->db->query($sql);

			if (is_object($rs)) {
				while ($rw = $rs->fetch_assoc()) {
					// Break MysQL 'Type' var into type, length and attributes
					$type		= (strpos($rw['Type'], '(') !== false) ? substr($rw['Type'], 0, strpos($rw['Type'], '(')) : $rw['Type'];
					$length		= substr($rw['Type'], strpos($rw['Type'], '(') + 1);
					$length		= substr($length, 0, strpos($length, ')'));
					$attributes	= (strpos($rw['Type'], ' ') !== false) ? substr($rw['Type'], (strpos($rw['Type'], ' ') + 1)) : null;

					// Define index
					switch($rw['Key']) {
						case 'PRI' :
							$index = 'PRIMARY';
							break;

						case 'UNI' :
							$index = 'UNIQUE';
							break;

						case 'MUL' :
							$index = 'INDEX';
							break;

						default:
							$index = null;
							break;
					}

					// Define foreign key
					$foreignKey = $this->GetColumnConstraint($table, $rw['Field']);

					$result[$rw['Field']] = array(
						'name'			=> $rw['Field'],
						'rename'		=> null,
						'type'			=> strtoupper($type),
						'length'		=> (int) $length,
						'attributes'	=> strtoupper($attributes),
						'collation'		=> $rw['Collation'],
						'null'			=> (bool) ($rw['Null'] == 'YES'),
						'extra'			=> strtoupper($rw['Extra']),
						'default'		=> (($rw['Null'] == 'YES') && empty($rw['Default'])) ? 'null' : $rw['Default'],
						'index'			=> $index,
						'comments'		=> $rw['Comment'],
						'foreign'		=> $foreignKey
					);
				}
			}else{
				trigger_error($this->db->error, E_USER_ERROR);
			}

			self::$cache[$table] = $result;

			return $result;
		}

		/**
		 * Check if index exists on currently-defined table
		 *
		 * @param string $indexName Name of index
		 * @return bool
		 *
		 */
		public function IndexExists($indexName) {
			// Table not defined
			if (empty($this->table['name'])) {
				trigger_error("Table must be defined before adding columns", E_USER_ERROR);

				return false;
			}

			$sql  = "SHOW INDEXES FROM `" . $this->table['name'] . "` ";
			$sql .= "WHERE `Key_name` = '" . $indexName . "'";

			$rs = $this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			// Index doesn't exist, return false
			if ($rs->num_rows < 1)
				return false;

			return true;
		}

		/**
		 * Retrieve foreign key constraints for specified table - in Creator format
		 *
		 * @param string $table Table name OR table.column association
		 * @param string $column Column name (optional if table.column is used in $table parameter)
		 * @return array Array of foreign key constraints
		 *
		 */
		public function GetColumnConstraint($table, $column = null) {
			$result = array();

			// Get column name from $table if table.column is used
			if (empty($column) && strpos($table, '.') !== false) {
				$column = explode('.', $table);

				$column = $column[1];
			}

			$table	= esc($table);
			$column	= esc($column);

			if (empty($column))
				trigger_error("Required parameter 'column' was missing", E_USER_ERROR);

			$baseTable = "referential_constraints";
			$joinTable = "key_column_usage";

			// Retrieve foreign key data for specified column
			$data = Database()
				->Select("$baseTable.constraint_name")
				->Select("$baseTable.update_rule")
				->Select("$baseTable.delete_rule")
				->Select("$baseTable.referenced_table_name")
				->Select("$joinTable.referenced_column_name")
				->From("information_schema.$baseTable")
				->Join("information_schema.$joinTable")
				->On("constraint_schema", "constraint_schema")
				->On("constraint_name", "constraint_name")
				->Where("$baseTable.constraint_schema", $this->dbName)
				->Where("$baseTable.table_name", $table)
				->Where("$joinTable.column_name", $column)
				->Limit(1)
				->Get();

			// Format result array
			if (!empty($data))
				$result = array(
					'references'	=> $data['referenced_table_name'] . '.' . $data['referenced_column_name'],
					'delete'		=> $data['delete_rule'],
					'update'		=> $data['update_rule']
				);

			return $result;
		}
		#endregion

		#region Data retrieval
		/**
		 * Add SELECT parameter to query
		 *
		 * @param string $column Column to select - can be column name or table.column syntax
		 * @param string $column Infinite additional columns (optional)
		 * @return this
		 *
		 */
		public function Select($column) {
			$params = func_get_args();

			// Loop through function params
			foreach ($params as $param) {
				$select = null;

				if ($param == '*') {
					$select .= $param;
				}else{
					// Split param into `table`.`field`
					$param = str_replace('`', '', esc($param));
					$param = explode('.', $param);

					foreach ($param as $val) {
						$select .= '`' . $val . '`.';
					}

					$select = substr($select, 0, -1);

					foreach ($this->select as $existing) {
						$existing = explode('.', $existing);

						if ($existing[1] == '`' . $param[1] . '`')
							$select .= " AS `" . $param[0] . '.' . $param[1] . "`";
					}
				}

				// Add param to array of select fields
				$this->select[] = $select;
			}

			return $this;
		}

		/**
		 * Add FROM parameter to query
		 *
		 * @param string $table Table to select - can be table name or database.table syntax
		 * @return this
		 *
		 */
		public function From($table) {
			$table = str_replace('`', '', esc($table));
			$table = explode('.', $table);

			if (count($table) > 1)
				$this->from = '`' . $table[0] . '`.`' . $table[1] . '`';
			else
				$this->from = '`' . $table[0] . '`';

			return $this;
		}

		/**
		 * Add JOIN parameter to query
		 * (Mask for InnerJoin)
		 *
		 * @param mixed $table Table to select - can be table name or database.table syntax
		 * @return this
		 *
		 */
		public function Join($table) {
			return $this->InnerJoin($table);
		}

		/**
		 * Add INNER JOIN parameter to JOIN query section
		 *
		 * @param mixed $table Table to select - can be table name or database.table syntax
		 * @return this
		 *
		 */
		public function InnerJoin($table) {
			$table = str_replace('`', '', esc($table));
			$table = explode('.', $table);

			if (count($table) > 1)
				$this->join[] = 'INNER JOIN `' . $table[0] . '`.`' . $table[1] . '`';
			else
				$this->join[] = 'INNER JOIN `' . $table[0] . '`';

			return $this;
		}

		/**
		 * Add LEFT JOIN parameter to JOIN query section
		 *
		 * @param mixed $table Table to select - can be table name or database.table syntax
		 * @return this
		 *
		 */
		public function LeftJoin($table) {
			$table = str_replace('`', '', esc($table));
			$table = explode('.', $table);

			if (count($table) > 1)
				$this->join[] = 'LEFT JOIN `' . $table[0] . '`.`' . $table[1] . '`';
			else
				$this->join[] = 'LEFT JOIN `' . $table[0] . '`';

			return $this;
		}

		/**
		 * Add RIGHT JOIN parameter to JOIN query section
		 *
		 * @param mixed $table Table to select - can be table name or database.table syntax
		 * @return this
		 *
		 */
		public function RightJoin($table) {
			$table = str_replace('`', '', esc($table));
			$table = explode('.', $table);

			if (count($table) > 1)
				$this->join[] = 'RIGHT JOIN `' . $table[0] . '`.`' . $table[1] . '`';
			else
				$this->join[] = 'RIGHT JOIN `' . $table[0] . '`';

			return $this;
		}

		/**
		 * Add ON parameter to the last defined JOIN
		 * Usually called directly after one of the JOIN methods
		 * eg. Database->Join(table)->On(table1, table2)
		 *
		 * @param mixed $baseColumn The base column to join target to (uses base table defined in FROM query section if table.column syntax is not used)
		 * @param mixed $targetColumn The target column to join base to (uses table from last join added if table.column syntax is not used)
		 * @return this
		 *
		 */
		public function On($baseColumn, $targetColumn) {
			// Define array key for current join
			$joinKey = count($this->join) - 1;

			// Begin ON query section
			$separator = (strpos($this->join[$joinKey], 'ON') === false) ? " ON (" : " AND ";
			$join = $this->join[$joinKey] . $separator;

			// Define join target table from start of this join section
			$joinTable = explode(' ', $this->join[$joinKey]);
			$joinTable = explode('.', $joinTable[2]);
			$joinTable = (count($joinTable) > 1) ? $joinTable[1] : $joinTable[0];

			#region Add base table predicate
			$base = explode('.', $baseColumn);

			if (count($base) == 1) {
				// Shorthand method - use base table defined by $this->from
				$table = str_replace('`', '', esc($base[0]));

				$from = explode('.', $this->from);
				$from = (count($from) > 1) ? $from[1] : $from[0];

				$join .= $from . '.`' . $table . '`';
			}else{
				// Full method - define what table.column to join target table to
				foreach ($base as $table) {
					$table = str_replace('`', '', esc($table));

					$join .= ' `' . $table . '`.';
				}

				$join = substr($join, 0, -1);
			}
			#endregion

			$join .= " = ";

			#region Add join target table predicate
			$target = explode('.', $targetColumn);

			if (count($target) == 1) {
				// Shorthand method - use target table defined by the last join added
				$table = str_replace('`', '', esc($target[0]));

				$join .= $joinTable . '.`' . $table . '`';
			}else{
				// Full method - define what table.column to join target table to
				foreach ($target as $table) {
					$table = str_replace('`', '', esc($table));

					$join .= ' `' . $table . '`.';
				}

				$join = substr($join, 0, -1);
			}
			#endregion

			// Replace initial started join with fully-build query section
			$this->join[$joinKey] = $join;

			return $this;
		}

		/**
		 * Execute query and return result set
		 *
		 * @return array Result array
		 *
		 */
		public function Get($format = 'assoc') {
			$result = array();

			if (!$this->TableExists($this->from)) {
				Warning("Table '$this->from' does not exist");

				$this->Reset();

				return $result;
			}

			$sql = $this->BuildQuery();

			if (key_exists(md5($sql), self::$cache)) {
				// Retrieve cached result
				$result = self::$cache[ md5($sql) ];
			}else{
				// Execute query
				$rs = $this->db->query($sql);

				if (is_object($rs)) {
					if ($format == 'object') {
						while ($rw = $rs->fetch_object()) {
							$result[] = $rw;
						}
					}else{
						while ($rw = $rs->fetch_assoc()) {
							$result[] = $rw;
						}
					}
				}else{
					trigger_error($this->db->error, E_USER_ERROR);
				}

				self::$cache[ md5($sql) ] = $result;
			}

			$this->Reset();

			return (count($result) > 1 || empty($result)) ? $result : $result[0];
		}
		#endregion

		#region Data manipulation
		public function Set($column, $value) {
			$column	= str_replace('`', '', esc($column));
			$value	= esc($value);

			$this->set[$column] = $value;

			return $this;
		}

		public function Insert() {
			// Table not defined
			if (empty($this->table['name'])) {
				trigger_error("Table not defined", E_USER_ERROR);

				return false;
			}

			$sql = "INSERT INTO `" . $this->table['name'] . "` (";

			// Define affected columns
			foreach ($this->set as $key => $val) {
				$sql .= "`$key`, ";
			}

			$sql = substr($sql, 0, -2) . ") VALUES (";

			// Define column values
			foreach ($this->set as $key => $val) {
				$sql .= "'$val', ";
			}

			$sql = substr($sql, 0, -2) . ")";

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			$insertId = $this->db->insert_id;

			$this->Reset();

			return $insertId;
		}

		public function Update() {
			// Table not defined
			if (empty($this->table['name'])) {
				trigger_error("Table not defined", E_USER_ERROR);

				return false;
			}

			// Where clauses not defined
			if (empty($this->where)) {
				trigger_error("Where clauses not defined", E_USER_ERROR);

				return false;
			}

			$sql = "UPDATE `" . $this->table['name'] . "` SET ";

			// Define column values to update
			foreach ($this->set as $key => $val) {
				$sql .= "`$key`='$val', ";
			}

			$sql = substr($sql, 0, -2) . " ";

			$sql .= $this->BuildWhere();
			$sql .= $this->BuildLimit();

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			$this->Reset();

			return true;
		}
		#endregion

		#region Clauses
		/**
		 * Add WHERE `column` = 'value' clause to query
		 *
		 * @param string $column Column to check - can be column name or table.column syntax
		 * @param string $value Value to check column for
		 * @return this
		 *
		 */
		public function Where($column, $value) {
			$value = esc($value);

			// Split column into `table`.`column`
			$column = str_replace('`', '', esc($column));
			$column = explode('.', $column);

			$where = null;

			foreach ($column as $val) {
				$where .= '`' . $val . '`.';
			}

			$where = substr($where, 0, -1);

			// Add param to array of where clauses
			if (empty($value) || strtoupper($value) == 'NULL')
				$this->where[] = "$where IS NULL";
			else
				$this->where[] = "$where = '$value'";

			return $this;
		}

		/**
		 * Add WHERE `column` != 'value' clause to query
		 *
		 * @param string $column Column to check - can be column name or table.column syntax
		 * @param string $value Value to check column for
		 * @return this
		 *
		 */
		public function WhereNot($field, $value) {
			$value = esc($value);

			// Split field into `table`.`field`
			$field = str_replace('`', '', esc($field));
			$field = explode('.', $field);

			$where = null;

			foreach ($field as $val) {
				$where .= '`' . $val . '`.';
			}

			$where = substr($where, 0, -1);

			// Add param to array of where clauses
			if (is_null($value) || strtoupper($value) == 'NULL')
				$this->where[] = "$where <> NULL";
			else
				$this->where[] = "$where != '$value'";

			return $this;
		}

		/**
		 * Add WHERE `column` LIKE '%value%' clause to query
		 *
		 * @param string $column Column to check - can be column name or table.column syntax
		 * @param string $value Value to check column for
		 * @return this
		 *
		 */
		public function WhereLike($field, $value) {
			$value = esc($value);

			// Split field into `table`.`field`
			$field = str_replace('`', '', esc($field));
			$field = explode('.', $field);

			$where = null;

			foreach ($field as $val) {
				$where .= '`' . $val . '`.';
			}

			$where = substr($where, 0, -1);

			// Add param to array of where clauses
			$this->where[] = "$where LIKE '%$value%'";

			return $this;
		}

		/**
		 * Add WHERE `column` LIKE 'value%' clause to query
		 *
		 * @param string $column Column to check - can be column name or table.column syntax
		 * @param string $value Value to check column for
		 * @return this
		 *
		 */
		public function WhereBeforeLike($field, $value) {
			$value = esc($value);

			// Split field into `table`.`field`
			$field = str_replace('`', '', esc($field));
			$field = explode('.', $field);

			$where = null;

			foreach ($field as $val) {
				$where .= '`' . $val . '`.';
			}

			$where = substr($where, 0, -1);

			// Add param to array of where clauses
			$this->where[] = "$where LIKE '$value%'";

			return $this;
		}

		/**
		 * Add WHERE `column` LIKE '%value' clause to query
		 *
		 * @param string $column Column to check - can be column name or table.column syntax
		 * @param string $value Value to check column for
		 * @return this
		 *
		 */
		public function WhereAfterLike($field, $value) {
			$field = str_replace('`', '', esc($field));
			$value = esc($value);

			// Split field into `table`.`field`
			$field = str_replace('`', '', esc($field));
			$field = explode('.', $field);

			$where = null;

			foreach ($field as $val) {
				$where .= '`' . $val . '`.';
			}

			$where = substr($where, 0, -1);

			// Add param to array of where clauses
			$this->where[] = "$where LIKE '%$value'";

			return $this;
		}

		/**
		 * Add WHERE `column` IN (values) clause to query
		 *
		 * @param string $column Column to check - can be column name or table.column syntax
		 * @param mixed $values CSV string or array of values to check column for
		 * @return this
		 *
		 */
		public function WhereIn($column, $values) {
			// Split column into `table`.`column`
			$column = str_replace('`', '', esc($column));
			$column = explode('.', $column);

			$where = null;

			foreach ($column as $val)
				$where .= '`' . $val . '`.';

			$where = substr($where, 0, -1);

			#region Convert CSV string to consistent array
			if (!is_array($values)) {
				$tempValues = explode(',', $values);

				$values = array();

				foreach ($tempValues as $value) {
					$value = trim($value);

					if (strpos($value, "'") === 0 || strpos($value, '"' === 0))
						$value = substr($value, 1, -1);

					$values[] = esc($value);
				}
			}
			#endregion

			$csv = null;

			foreach ($values as $value)
				$csv .= "'" . esc($value) . "',";

			$csv = substr($csv, 0, -1);

			$this->where[] = "$where IN ($csv)";

			return $this;
		}

		/**
		 * Add WHERE `column` NOT IN (values) clause to query
		 *
		 * @param string $column Column to check - can be column name or table.column syntax
		 * @param mixed $values CSV string or array of values to check column for
		 * @return this
		 */
		public function WhereNotIn($column, $values) {
			// Split column into `table`.`column`
			$column = str_replace('`', '', esc($column));
			$column = explode('.', $column);

			$where = null;

			foreach ($column as $val)
				$where .= '`' . $val . '`.';

			$where = substr($where, 0, -1);

			#region Convert CSV string to consistent array
			if (!is_array($values)) {
				$tempValues = explode(',', $values);

				$values = array();

				foreach ($tempValues as $value) {
					$value = trim($value);

					if (strpos($value, "'") === 0 || strpos($value, '"' === 0))
						$value = substr($value, 1, -1);

					$values[] = esc($value);
				}
			}
			#endregion

			$csv = null;

			foreach ($values as $value)
				$csv .= "'" . esc($value) . "',";

			$csv = substr($csv, 0, -1);

			$this->where[] = "$where NOT IN ($csv)";

			return $this;
		}

		/**
		 * Add WHERE FIND_IN_SET(value, `column`) clause to query
		 *
		 * @param string $column Column to check - can be column name or table.column syntax
		 * @param string $value Value to check column for
		 * @return this
		 *
		 */
		public function WhereInSet($column, $value) {
			$value = esc($value);

			// Split column into `table`.`column`
			$column = str_replace('`', '', esc($column));
			$column = explode('.', $column);

			$where = null;

			foreach ($column as $val)
				$where .= '`' . $val . '`.';

			$where = substr($where, 0, -1);

			$this->where[] = "FIND_IN_SET('$value', $where)";

			return $this;
		}

		/**
		 * Adds OR clause after last-added WHERE clause
		 *
		 * @return this
		 *
		 */
		public function Orr() {
			$this->where[] = "OR";

			return $this;
		}

		/**
		 * Groups previously-added clauses and begins another clause group compared by AND
		 *
		 * @return this
		 *
		 */
		public function GroupAnd() {
			$this->where[] = ") AND (";

			return $this;
		}

		/**
		 * Groups previously-added clauses and begins another clause group compared by OR
		 *
		 * @return this
		 *
		 */
		public function GroupOr() {
			$this->where[] = ") OR (";

			return $this;
		}
		#endregion

		#region Extras
		/**
		 * Defines GROUP BY query section
		 *
		 * @param mixed $column Column name to group results by
		 * @return this
		 *
		 */
		public function GroupBy($column) {
			// Split field into `table`.`field`
			$column = str_replace('`', '', esc($column));
			$column = explode('.', $column);

			$group = null;

			foreach ($column as $val) {
				$group .= '`' . $val . '`.';
			}

			$group = substr($group, 0, -1);

			// Add param to array of where clauses
			$this->group = "$group";

			return $this;
		}

		/**
		 * Adds ORDER BY query parameters - can add multiple columns as parameters in $column, $direction order (defaults to ASC if defined in $column, $column order)
		 *
		 * @param mixed $column Column name - can be column name or table.column syntax
		 * @param mixed $direction Direction to order results - ASC or DESC
		 * @param mixed $column Additional column name - can be column name or table.column syntax
		 * @param mixed $direction Additional direction to order results - ASC or DESC
		 * @return this
		 *
		 */
		public function Order($column, $direction) {
			$params = func_get_args();

			$columns	= array();
			$directions	= array();

			$directionTypes = array("ASC", "DESC");

			// Separate params into fields and directions
			$i = 0;
			foreach ($params as $key => $val) {
				if (!in_array(strtoupper($val), $directionTypes)) {
					$i++;

					// Split field into `table`.`field`
					$val = str_replace('`', '', esc($val));
					$val = explode('.', $val);

					$order = null;

					foreach ($val as $val2) {
						$order .= '`' . $val2 . '`.';
					}

					$order = substr($order, 0, -1);

					$columns[$i] = $order;
				}else{
					$directions[$i] = strtoupper($val);
				}
			}

			// Add sql to order property array
			foreach ($columns as $key => $val) {
				if (!empty($directions[$key]))
					$this->order[] = $val . ' ' . $directions[$key];
				else
					$this->order[] = $val . ' ASC';
			}

			return $this;
		}

		/**
		 * Defines LIMIT query section
		 *
		 * @param mixed $limit Number of results to limit to
		 * @param mixed $offset Number of records to offset results by
		 * @return this
		 *
		 */
		public function Limit($limit, $offset = null) {
			if (!empty($to))
				$this->limit = "$offset, $limit";
			else
				$this->limit = $limit;

			return $this;
		}
		#endregion

		#region Table manipulation
		/**
		 * Define table to update or create
		 *
		 * @param mixed $params Either table name as string or table parameter array
		 * @return this
		 *
		 */
		public function Table($params) {
			$params = esc($params);

			// Only table name was passed as string
			if (is_string($params))
				$params = array(
					"name"	=> $params
				);

			// Define default params
			$defaultParams = array(
				"name"		=> null,
				"engine"	=> "InnoDB",
				"collation"	=> "utf8_unicode_ci",
				"comments"	=> null
			);

			// Combine passed params with default set and save to object
			$params =  array_merge($defaultParams, $params);

			// Format and typecast params
			$params = array(
				"name"		=> $params['name'],
				"engine"	=> $params['engine'],
				"collation"	=> strtolower($params['collation']),
				"comments"	=> $params['comments']
			);

			$this->table = $params;

			return $this;
		}

		/**
		 * Add column to defined table
		 *
		 * @param array $params Column parameter array
		 * @return this
		 *
		 */
		public function Column($params) {
			$params = esc($params);

			$defaultLength = array(
				"tinyint"	=> 4,
				"int"		=> 10,
				"varchar"	=> 127
			);


			// Define default params
			$defaultParams = array(
				"name"			=> null,
				"rename"		=> null,
				"type"			=> "int",
				"length"		=> null,
				"attributes"	=> null,
				"collation"		=> null,
				"null"			=> false,
				"extra"			=> null,
				"default"		=> null,
				"index"			=> null,
				"comments"		=> null,
				"foreign"		=> array()
			);

			// Combine passed params with default set and save to object
			$params = array_merge($defaultParams, $params);

			// Format and typecast params
			$params = array(
				"name"			=> $params['name'],
				"rename"		=> $params['rename'],
				"type"			=> strtoupper($params['type']),
				"length"		=> (empty($params['length']) && !empty($defaultLength[ $params['type'] ])) ? $defaultLength[ $params['type'] ] : $params['length'],
				"attributes"	=> strtoupper($params['attributes']),
				"collation"		=> strtolower($params['collation']),
				"null"			=> (bool) $params['null'],
				"extra"			=> strtoupper($params['extra']),
				"default"		=> (empty($params['default']) && $params['null']) ? 'null' : $params['default'],
				"index"			=> strtoupper($params['index']),
				"comments"		=> $params['comments'],
				"foreign"		=> $params['foreign']
			);

			// Format foreign key params
			if (!empty($params['foreign']))
				$params['foreign'] = array(
					"references"	=> $params['foreign']['references'],
					"delete"		=> strtoupper($params['foreign']['delete']),
					"update"		=> strtoupper($params['foreign']['update']),
				);

			$this->columns[$params['name']] = $params;

			return $this;
		}

		/**
		 * Create table using defined table properties
		 *
		 * @return bool
		 *
		 */
		public function CreateTable() {
			$indexes = array();

			$sql = "CREATE TABLE IF NOT EXISTS `" . $this->table['name'] . "` (";

			// Define columns
			foreach ($this->columns as $column) {
				// Define column properties SQL
				$sql .= $this->GenerateColumnSQL($column) . ", ";

				// Define column indexes and foreign keys
				$columnIndexes = $this->GenerateColumnIndexes($column);

				$indexes = array_merge($indexes, $columnIndexes);
			}

			// Define column indexes
			foreach ($indexes as $index) {
				$sql .= $index . ", ";
			}

			$sql = substr($sql, 0, -2) . ") ";

			// Define engine
			$sql .= "ENGINE=" . $this->table['engine'] . " ";

			// Define collation
			$charSet = substr($this->table['collation'], 0, strpos($this->table['collation'], '_'));
			$sql .= "DEFAULT CHARSET=$charSet COLLATE=" . $this->table['collation'] . " ";

			// Define AUTO_INCREMENT position
			if (strpos($sql, 'AUTO_INCREMENT') !== false)
				$sql .= "AUTO_INCREMENT=1 ";

			$sql = substr($sql, 0, -1);

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			$this->Reset();

			return true;
		}

		/**
		 * Update table using defined table properties
		 *
		 * @return bool
		 *
		 */
		public function UpdateTable() {
			$indexes = array();

			$tableColumns = $this->GetTableColumns($this->table['name']);

			// Define columns to update/add
			$toAdd		= array();
			$toUpdate	= array();

			$addOrder = "FIRST";

			$i = 0;

			foreach ($this->columns as $key => $val) {
				$columnName = !empty($val['rename']) ? $val['rename'] : $key;

				if (!isset($tableColumns[$columnName])) {
					// Add new column
					$toAdd[$i]			= $val;
					$toAdd[$i]['order']	= $addOrder;
				}else{
					// Update existing column
					$colDiff		= array_diff_assoc($val, $tableColumns[$key]);
					$foreignDiff	= array_diff_assoc($val['foreign'], $tableColumns[$key]['foreign']);

					if (!empty($colDiff) || (!empty($foreignDiff) || !empty($tableColumns[$key]['foreign'])))
						$toUpdate[$i] = array_merge($val, $colDiff);

					if (!empty($foreignDiff))
						$toUpdate[$i]['foreign'] = array_merge($val['foreign'], $foreignDiff);
				}

				$addOrder = "AFTER `" . $key . "`";

				$i++;
			}

			// Define columns to delete
			$toDelete = array();

			foreach ($tableColumns as $key => $val) {
				if (!isset($this->columns[$key]))
					$toDelete[] = $key;
			}

			// Delete columns
			if (!empty($toDelete))
				$this->DeleteColumn($toDelete);

			// Update columns
			if (!empty($toUpdate))
				$this->UpdateColumn($toUpdate);

			// Add columns
			if (!empty($toAdd))
				$this->AddColumn($toAdd);

			if (isset($this->tableCache[$this->table['name']]))
				unset($this->tableCache[$this->table['name']]);
		}

		/**
		 * Delete column from defined table
		 *
		 * @param mixed $params Column name as string or array of column names
		 * @return bool
		 *
		 */
		public function DeleteColumn($params) {
			// Table not defined
			if (empty($this->table['name'])) {
				trigger_error("Table must be defined before adding columns", E_USER_ERROR);

				return false;
			}

			$tableColumns = $this->GetTableColumns($this->table['name']);

			$sql  = "ALTER TABLE `" . $this->table['name'] . "` ";

			// Single column passed as string, convert to array
			if (!is_array($params))
				$params = array($params);

			// Build SQL
			foreach ($params as $column) {
				$sql .= "DROP `$column`, ";

				if (!empty($tableColumns[$column]['foreign']))
					$this->DropForeignKey("fk_" . $column);
			}

			$sql = substr($sql, 0, -2);

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			return true;
		}

		/**
		 * Update column on defined table
		 *
		 * @param array $params Single column parameter array or array of column parameter arrays
		 * @return bool
		 *
		 */
		public function UpdateColumn($params) {
			// Table not defined
			if (empty($this->table['name'])) {
				trigger_error("Table must be defined before adding columns", E_USER_ERROR);

				return false;
			}

			$keys = array_keys($params);

			if (is_int($keys[0])) {
				// Array of multiple columns to update has been passed
				foreach ($params as $param) {
					if (isset($param['name'])) {
						// Call this method for single column
						$this->UpdateColumn($param);
					}else{
						// Invalid parameters
						trigger_error("Invalid column array was passed", E_USER_ERROR);

						return false;
					}
				}
			}else{
				// Single column
				$currentTable = $this->GetTableColumns($this->table['name']);

				$sql  = "ALTER TABLE `" . $this->table['name'] . "` ";

				$columnSQL = $this->GenerateColumnSQL($params);

				if (!empty($params['rename']))
					$columnSQL = str_replace('`' . $params['name'] . '`', '`' . $params['name'] . '` `' . $params['rename'] . '`', $columnSQL);
				else
					$columnSQL = str_replace('`' . $params['name'] . '`', '`' . $params['name'] . '` `' . $params['name'] . '`', $columnSQL);

				$sql .= "CHANGE $columnSQL";

				// Remove foreign key and update column foreign key params
				if (!empty($currentTable[$params['name']]['foreign']) || !empty($params['foreign'])) {
					$difference = array_diff_assoc($currentTable[$params['name']]['foreign'], $params['foreign']);

					if (!empty($difference) && !empty($params['foreign']))
						$params['foreign'] = array_merge($currentTable[$params['name']]['foreign'], $params['foreign']);

					if (!empty($currentTable[$params['name']]['foreign']))
						$this->DropForeignKey("fk_" . $this->table['name'] . '_' . $params['name']);
				}

				// Remove indexes
				$this->DropIndex($params['name']);

				// Execute main alter column query
				$this->db->query($sql);

				// Log MySQL error
				if (!empty($this->db->error)) {
					trigger_error($this->db->error, E_USER_ERROR);

					return false;
				}

				if (!empty($params['index']))
					$this->AddColumnIndex($params);

				if (!empty($params['foreign']))
					$this->AddForeignKey($params);
			}

			return true;
		}

		/**
		 * Add column to defined table
		 *
		 * @param array $params Single column parameter array or array of column parameter arrays
		 * @return bool
		 *
		 */
		public function AddColumn($params) {
			$indexes = array();

			// Table not defined
			if (empty($this->table['name'])) {
				trigger_error("Table must be defined before adding columns", E_USER_ERROR);

				return false;
			}

			$sql  = "ALTER TABLE `" . $this->table['name'] . "` ";

			$keys = array_keys($params);

			if (is_int($keys[0])) {
				// Array of multiple columns to add has been passed
				foreach ($params as $param) {
					if (isset($param['name'])) {
						$sql .= "ADD " . $this->GenerateColumnSQL($param) . " ";

						if (!empty($param['order']))
							$sql .= $param['order'] . " ";

						// Define column indexes and foreign keys
						$columnIndexes = $this->GenerateColumnIndexes($param);
						$indexes = array_merge($indexes, $columnIndexes);

						$sql = substr($sql, 0, -1) . ", ";
					}else{
						trigger_error("Invalid parameters were passed", E_USER_ERROR);

						return false;
					}
				}

				$sql = substr($sql, 0, -2);
			}else{
				// Single column array
				$sql .= "ADD " . $this->GenerateColumnSQL($params) . " ";

				if (!empty($params['order']))
					$sql .= $params['order'] . " ";

				// Define column indexes and foreign keys
				$columnIndexes = $this->GenerateColumnIndexes($params);
				$indexes = array_merge($indexes, $columnIndexes);

				$sql = substr($sql, 0, -1);
			}

			// Define column indexes
			if (!empty($indexes)) {
				foreach ($indexes as $index) {
					if (strpos($index, 'CONSTRAINT') === false)
						$sql .= ", ";

					$sql .= "ADD $index, ";
				}

				$sql = substr($sql, 0, -2);
			}

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			return true;
		}

		/**
		 * Drop an index from currently-defined table
		 *
		 * @param string $indexName Name of index to remove
		 * @return bool
		 *
		 */
		public function DropIndex($indexName) {
			if (empty($this->table['name'])) {
				trigger_error("Table has not been defined", E_USER_ERROR);

				return false;
			}

			$indexName = esc( str_replace('`', '', $indexName) );

			// Return false if index doesn't exist
			if (!$this->IndexExists($indexName))
				return true;

			$sql = "DROP INDEX `$indexName` ON `" . $this->table['name'] . "`";

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			return true;
		}

		/**
		 * Add index to specified column
		 *
		 * @param array $params Column parameter array
		 * @return bool
		 *
		 */
		public function AddColumnIndex($params) {
			// Table not defined
			if (empty($this->table['name'])) {
				trigger_error("Table must be defined before adding columns", E_USER_ERROR);

				return false;
			}

			$indexes = $this->GenerateColumnIndexes($params);

			$sql  = "ALTER TABLE `" . $this->table['name'] . "` ";
			$sql .= "ADD " . $indexes[0];

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			return true;
		}

		/**
		 * Drop foreign key for specified index name
		 *
		 * @param string $indexName Name of index
		 * @return bool
		 *
		 */
		public function DropForeignKey($indexName) {
			// Table not defined
			if (empty($this->table['name'])) {
				trigger_error("Table has not been defined", E_USER_ERROR);

				return false;
			}

			$indexName	= esc( str_replace('`', '', $indexName) );

			$sql = "ALTER TABLE `" . $this->table['name'] . "` DROP FOREIGN KEY `$indexName`";

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}else{
				$this->DropIndex($indexName);
			}

			return true;
		}

		/**
		 * Add foreign key to table specified by Table()
		 *
		 * @param array $params Full column param array
		 * @return bool
		 *
		 */
		public function AddForeignKey($params) {
			if (empty($this->table['name'])) {
				trigger_error("Table has not been defined", E_USER_ERROR);

				return false;
			}

			// Invalid parameters
			if (empty($params['foreign']['references'])) {
				trigger_error("Invalid parameter array passed", E_USER_ERROR);

				return false;
			}

			$indexes = $this->GenerateColumnIndexes($params);

			$sql = "ALTER TABLE `" . $this->table['name'] . "` ADD " . $indexes[1];

			$this->db->query($sql);

			// Log MySQL error
			if (!empty($this->db->error)) {
				trigger_error($this->db->error, E_USER_ERROR);

				return false;
			}

			return true;
		}
		#endregion

		/**
		 * Intelligent method for executing database query, based on set properties
		 *
		 * @return bool
		 *
		 */
		public function Execute() {
			if (!empty($this->set) && !empty($this->where))
				return $this->Update();

			elseif (!empty($this->set) && empty($this->where))
				return $this->Insert();

			elseif (!$this->TableExists($this->table['name']))
				return $this->CreateTable();

			else
				return $this->UpdateTable();
		}
		#endregion
	}