<?php
	#region Core

	/**
	 * Require a class and initialise it
	 *
	 * @param string $className Name of class to init
	 * @param string $path Include path relative to /system/
	 *
	 * @return object | bool Requested class object or false if class doesn't exist
	 */
	function InitClass($className, $path = "core") {
		$className = strToCamel($className);
		$classFile = strToPath($className);

		$path = ROOT . "/system/" . $path . "/" . $classFile . ".php";

		if (!file_exists($path)) {
			trigger_error($className . " class file does not exist at '" . $path . "'");
			return false;
		} else {
			if (class_exists($className) === false) {
				require($path);

				if (class_exists($className)) {
					$class = new $className();

					return $class;
				} else {
					trigger_error($className . " class does not exist in class file", E_USER_ERROR);
					return false;
				}
			} else {
				$class = new $className();

				return $class;
			}
		}
	}

	function HTTP($code) {
		switch ($code) {
			case "404":
				header("HTTP/1.0 404 Not Found");
				echo "404: Page not found";
				break;

			default:
				throw new Exception("HTTP code not yet implemented");
				break;
		}

		exit();
	}

	#endregion Core

	#region Engines

	/**
	 * Config engine
	 * @param string $item Config item if performing Item shortcut
	 * @return Config | string if performing Item shortcut
	 */
	function Config($item = null) {
		global $CONFIG;

		if (!empty($item)) {
			return $CONFIG->$item;
		}

		return $CONFIG;
	}

	/**
	 * Database engine
	 *
	 * @return Database
	 */
	function Database() {
		global $DATABASE;

		return clone $DATABASE;
	}

	/**
	 * Loading engine
	 *
	 * @param string $path Require path if performing Require shortcut
	 * @return Loader | boolean if performing Require shorcut
	 */
	function Load($path = null) {
		global $LOADER;

		if (!empty($path)) {
			$LOADER->Inc($path);
			return true;
		}

		return $LOADER;
	}

	/**
	 * Rendering engine
	 *
	 * @return Renderer
	 */
	function Render() {
		global $RENDERER;

		return $RENDERER;
	}

	/**
	 * Routing engine
	 * @param string $action Controller action to reroute to if using shortcut
	 * @param string $controller Controller to reroute to if using shortcut
	 * @return Router | boolean if performing ReRoute shortcut
	 */
	function Route($action = null, $controller = null) {
		global $ROUTER;

		if (!empty($action)) {
			$ROUTER->ReRoute($action, $controller);
			return true;
		}

		return $ROUTER;
	}

	#endregion Engines

	#region Helpers

	/**
	 * Escape and trim data for use in an SQL query
	 *
	 * @param mixed $data Array or string to escape
	 * @return mixed Returns an escaped version of the data passed
	 */
	function esc($data) {
		$escData = $data;

		if (is_array($data)) {
			$escData = array();

			foreach ($data as $key => $val) {
				$escData[$key] = esc($val);
			}
		} else {
			if (ini_get('magic_quotes_gpc') !== false) {
				$escData = stripslashes($escData);
			}

			$escData = Database()->Esc(trim($escData));
		}

		return $escData;
	}

	/**
	 * Check if $_POST contains data
	 *
	 * @return bool
	 */
	function isPosted() {
		if (!empty($_POST)) {
			return true;
		}

		return false;
	}

	/**
	 * 302 Redirect
	 *
	 * @param string $path Path to redirect to
	 */
	function redirect($path = "") {
		if (empty($path)) {
			$path = $_SERVER["REQUEST_URI"];
		}

		header("Location: " . $path);
	}

	#endregion

	#region Parsers

	/**
	 * Convert first character of string to lowercase
	 *
	 * @param string $string String to convert
	 *
	 * @return string $string
	 */
	function firstToLower($string) {
		$first = strtolower(substr($string, 0, 1));

		$result = $first . substr($string, 1);

		return $result;
	}

	/**
	 * Convert first character of string to uppercase
	 *
	 * @param string $string String to convert
	 *
	 * @return string $string
	 */
	function firstToUpper($string) {
		$first = strtoupper(substr($string, 0, 1));

		$result = $first . substr($string, 1);

		return $result;
	}

	/**
	 * Convert hyphen-separated lowercase string to CamelCase
	 *
	 * @param string $string String to convert
	 *
	 * @return string Converted string
	 */
	function strToCamel($string) {
		$result = null;

		foreach (explode("-", $string) as $segment) {
			$result .= firstToUpper($segment);
		}

		return $result;
	}

	/**
	 * Convert CamelCase string to hyphen-separated lowercase
	 *
	 * @param string $string String to convert
	 *
	 * @return string Converted string
	 */
	function strToPath($string) {
		$string = firstToLower($string);

		$result = preg_replace_callback("/([A-Z]+?)/",
			function ($matches) {
				return "-" . strToLower($matches[1]);
			}, $string);

		return $result;
	}

	#endregion Parsers