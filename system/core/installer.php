<?php
	class Installer {
		protected $currentVersion = 0;

		public function __construct($displayName = null) {
			if (!Database()->TableExists("example_controllers")) {
				$this->InstallControllers();
			}

			$controller = Database()
				->From("example_controllers")
				->Where("name", Route()->controllerName)
				->Limit(1)
				->Get();

			if(!empty($controller) && $controller["version"] == $this->currentVersion) {
				// Controller version is up to date - no need to run installer
				return true;
			}

			$this->Init();

			$displayName = !empty($displayName) ? $displayName : Route()->controllerName;

			$updateVersion = Database()
				->Table("example_controllers")
				->Set("name", Route()->controllerName)
				->Set("displayName", $displayName)
				->Set("version", $this->currentVersion);

			if (!empty($controller)) {
				$updateVersion->Where("id", $controller["id"]);
			}

			$updateVersion->Execute();
		}

		private function InstallControllers() {
			Database()
				->Table(array(
					"name"			=> "example_controllers",
					"comments"		=> "Core controllers table"
					))
				->Column(array(
					"name"			=> "id",
					"type"			=> "int",
					"length"		=> 11,
					"attributes"	=> "unsigned",
					"null"			=> false,
					"extra"			=> "auto_increment",
					"index"			=> "primary"
					))
				->Column(array(
					"name"			=> "name",
					"type"			=> "varchar",
					"length"		=> 255
					))
				->Column(array(
					"name"			=> "displayName",
					"type"			=> "varchar",
					"length"		=> 255
					))
				->Column(array(
					"name"			=> "version",
					"type"			=> "decimal",
					"length"		=> "5,1",
					"default"		=> 1
					))
				->Column(array(
					"name"			=> "timeUpdated",
					"type"			=> "timestamp",
					"default"		=> "CURRENT_TIMESTAMP",
					"extra"			=> "on update CURRENT_TIMESTAMP"
					))
				->Execute();
		}

		/**
		 * Initialise the installer
		 */
		protected function Init() {
			// Placeholder method - controller installer will override
		}
	}