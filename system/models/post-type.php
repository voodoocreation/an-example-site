<?php
	class PostType extends Model {
		protected $table = "example_post_types";

		protected $assocMulti = array (
			"Posts" => array("post.postTypeId", "this.id")
			);

		protected $validation = array (
			"name"	=> array (
				"type"		=> "text",
				"label"		=> "Name",
				"comment"	=> "A unique name for the post type"
			),
			"single"	=> array (
				"type"		=> "text",
				"label"		=> "Single name",
				"comment"	=> "The name of a single post of this type"
			),
			"plural"	=> array (
				"type"		=> "text",
				"label"		=> "Plural name",
				"comment"	=> "The name of multiple posts of this type"
			)
		);
	}