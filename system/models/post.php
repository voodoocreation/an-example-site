<?php
	class Post extends Model {
		protected $table = "example_posts";

		protected $assocSingle = array (
			"PostType" => array("post-type.id", "this.postTypeId")
			);

		protected $validation = array (
			"postTypeId"	=> array (
				"type"		=> "dropdown",
				"label"		=> "Post type",
				"comment"	=> "The type of post"
				),
			"slug"	=> array (
				"type"		=> "text",
				"label"		=> "Slug",
				"comment"	=> "A unique identifier used for creating links - lowercase alphanumeric characters and hyphens"
				),
			"title"	=> array (
				"type"		=> "text",
				"label"		=> "Title",
				"comment"	=> "The title of the post"
				),
			"description"	=> array (
				"type"		=> "text",
				"label"		=> "Description",
				"comment"	=> "Short description of the post - appears in search engines"
				),
			"content"	=> array (
				"type"		=> "html",
				"label"		=> "Post content",
				"comment"	=> "Full post content"
				)
		);
	}