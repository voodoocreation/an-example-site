<?php
	class User extends Model {
		protected $table = "example_users";

		protected $validation = array (
			"name"	=> array (
				"type"		=> "text",
				"label"		=> "Display name",
				"comment"	=> "The user's display name"
			),
			"username"	=> array (
				"type"		=> "text",
				"label"		=> "Username",
				"comment"	=> "The username used to log in"
			),
			"password"	=> array (
				"type"		=> "password",
				"label"		=> "Password",
				"comment"	=> "The password used to log in"
			)
		);
	}