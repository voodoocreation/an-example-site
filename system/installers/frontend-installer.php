<?php
	class Install extends Installer {
		protected $currentVersion = 1;

		protected function Init() {
			$this->InstallPostTypes();
			$this->InstallPosts();
		}

		private function InstallPostTypes() {
			// Create table
			Database()
				->Table(array(
					 "name"			=> "example_post_types"
					))
				->Column(array(
					"name"			=> "id",
					"type"			=> "int",
					"length"		=> 11,
					"attributes"	=> "unsigned",
					"null"			=> false,
					"extra"			=> "auto_increment",
					"index"			=> "primary"
					))
				->Column(array(
					"name"			=> "name",
					"type"			=> "varchar",
					"length"		=> 255,
					"index"			=> "unique"
					))
				->Column(array(
					"name"			=> "single",
					"type"			=> "varchar",
					"length"		=> 63
					))
				->Column(array(
					"name"			=> "plural",
					"type"			=> "varchar",
					"length"		=> 63
				 	))
				->Execute();

			$postTypes = Database()
				->Select("id")
				->From("example_post_types")
				->Limit(1)
				->Get();

			if (empty($postTypes)) {
				// Add 'post' type
				Database()
					->Table("example_post_types")
					->Set("name", "post")
					->Set("single", "Post")
					->Set("plural", "Posts")
					->Execute();

				// Add 'page' type
				Database()
					->Table("example_post_types")
					->Set("name", "page")
					->Set("single", "Page")
					->Set("plural", "Pages")
					->Execute();
			}
		}

		private function InstallPosts() {
			Database()
				->Table(array(
					"name"			=> "example_posts",
					"engine"		=> "MyISAM"
					))
				->Column(array(
					"name"			=> "id",
					"type"			=> "int",
					"length"		=> 11,
					"attributes"	=> "unsigned",
					"null"			=> false,
					"extra"			=> "auto_increment",
					"index"			=> "primary"
					))
				->Column(array(
					"name"			=> "postTypeId",
					"type"			=> "int",
					"length"		=> 11,
					"default"		=> 1,
					"attributes"	=> "unsigned",
					"null"			=> false,
					"index"			=> "index"
					))
				->Column(array(
					"name"			=> "slug",
					"type"			=> "varchar",
					"length"		=> 127,
					"index"			=> "unique"
					))
				->Column(array(
					"name"			=> "template",
					"type"			=> "varchar",
					"length"		=> 127,
					"default"		=> "default"
					))
				->Column(array(
					"name"			=> "class",
					"type"			=> "varchar",
					"length"		=> 127,
					"default"		=> "file"
					))
				->Column(array(
					"name"			=> "title",
					"type"			=> "varchar",
					"length"		=> 255
					))
				->Column(array(
					"name"			=> "description",
					"type"			=> "varchar",
					"length"		=> 156
					))
				->Column(array(
					"name"			=> "content",
					"type"			=> "text",
					))
				->Column(array(
					"name"			=> "timeUpdated",
					"type"			=> "timestamp",
					"default"		=> "CURRENT_TIMESTAMP",
					"extra"			=> "on update CURRENT_TIMESTAMP"
					))
				->Execute();
		}
	}