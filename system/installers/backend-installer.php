<?php
	class Install extends Installer {
		protected $currentVersion = 1;

		protected function Init() {
			$this->InstallUsers();
		}

		private function InstallUsers() {
			// Create table
			Database()
				->Table(array(
					 "name"			=> "example_users"
					))
				->Column(array(
					"name"			=> "id",
					"type"			=> "int",
					"length"		=> 11,
					"attributes"	=> "unsigned",
					"null"			=> false,
					"extra"			=> "auto_increment",
					"index"			=> "primary"
					))
				->Column(array(
					"name"			=> "name",
					"type"			=> "varchar",
					"length"		=> 127
					))
				->Column(array(
					"name"			=> "username",
					"type"			=> "varchar",
					"length"		=> 127,
					"index"			=> "unique"
					))
				->Column(array(
					"name"			=> "password",
					"type"			=> "varchar",
					"length"		=> 128
					))
				->Execute();
		}
	}