<?php
	class FrontendController extends Controller {
		#region Actions

		/**
		 * View breadcrumbs
		 */
		public function Breadcrumbs() {
			// Define crumbs array
			$crumbs = array(
				"/" => array(
					"title" => "Home"
					)
				);
			foreach (Route()->segments as $segment) {
				$page = Load()->Model("post")
					->Select("id")
					->Select("slug")
					->Select("title")
					->Where("slug", $segment)
					->Limit(1)
					->Get();

				if ($page->Count() > 0) {
					$path = "/" . $page->slug . "/";

					$crumbs[$path]["title"] = $page->title;
				}
			}

			// Add level to each crumb
			$count = count($crumbs);
			foreach ($crumbs as $path => $page) {
				$count -= 1;

				$level = null;
				if ($count > 0) {
					for($i = 1; $i <= $count; $i += 1) {
						$level .= " up";
					}

					$level = substr($level, 1);
				}

				$crumbs[$path]["level"] = $level;
			}

			$data["crumbs"] = $crumbs;

			Load()->View("breadcrumbs", $data);
		}

		/**
		 * View a post
		 */
		public function Post() {
			$slug = substr($_SERVER["REQUEST_URI"], 1, -1);
			$pageUrl = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

			Render()->Token("title", "Pages");

			Render()->Token("url", $pageUrl);

			if (!empty($slug) && $slug !== "/") {
				$page = Load()->Model("post")
					->Where("slug", $slug)
					->Limit(1)
					->Get();

				if ($page->Count() > 0) {
					Render()->Template($page->template);
					Render()->Token("title", $page->title);
					Render()->Token("description", $page->description);
					Render()->Token("content", $page->content);
					Render()->Token("class", $page->class);
				} else {
					HTTP("404");
				}
			} else {
				Render()->Template("home");
				Render()->Token("description", "The description for the home page.");
			}
		}

		/**
		 * View nav
		 */
		public function Nav() {
			Load()->View("nav");
		}

		#endregion Actions
	}