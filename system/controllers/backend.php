<?php
	class BackendController extends Controller {
		public function __construct() {
			parent::__construct();

			Render()->Template("backend");
			$this->LoginCheck();
		}

		#region Private



		#endregion Private

		#region Public

		#region Actions

		/**
		 * View account controls
		 */
		public function AccountControls() {
			$data["user"] = $this->user;
			Load()->View("account-controls", $data);
		}

		/**
		 * View index
		 */
		public function Index() {
			$this->Title("Example administrator");

			Render()->Token("class", "home");
			Load()->View("index");
		}

		/**
		 * View nav
		 */
		public function Nav() {
			Load()->View("nav");
		}

		public function Pages() {
			$this->Title("Pages");
			Render()->Token("class", "file");

			Load()->JS("ng-svc-pages.js");
			Load()->JS("ng-ctrl-pages.js");

			Load()->View("pages");
		}

		#region API actions

		public function Pages_GET() {
			$result = array();

			$pages = Load()->Model("post")
				->Where("postTypeId", 2)
				->Get();

			foreach ($pages as $page) {
				$result[] = array(
					"id"			=> $page->id,
					"slug"			=> $page->slug,
					"template"		=> $page->template,
					"class"			=> $page->class,
					"title"			=> $page->title,
					"description"	=> $page->description,
					"content"		=> $page->content
					);
			}

			Render()->JSON($result);
		}

		public function Page_POST() {
			$result = array();

			$post = json_decode(file_get_contents('php://input'));

			if (!empty($post)) {
				$page = Load()->Model("post");
				if (!empty($post->id)) {
					$page->id = $post->id;
				}
				$page->postTypeId	= 2;
				$page->slug			= $post->slug;
				$page->class		= $post->class;
				$page->title		= $post->title;
				$page->description	= $post->description;
				$page->content		= $post->content;
				$page->Save();

				if ($page->Count() > 0) {
					$result = array(
						"id"			=> $page->id,
						"slug"			=> $page->slug,
						"template"		=> $page->template,
						"class"			=> $page->class,
						"title"			=> $page->title,
						"description"	=> $page->description,
						"content"		=> $page->content
						);
				}
			}

			Render()->JSON($result);
		}

		#endregion API actions

		#endregion Actions

		#endregion Public
	}