  <div class="account-controls">
<? if (!empty($user)): ?>
   <div class="account-name"><i class="icon-user"></i> <?= $user->name ?></div>
   <div class="account-actions">
    <a href="/admin/logout/" class="btn"><i class="icon-enter"></i> Log out</a>
   </div>
<? else: ?>
   <div class="account-actions">
    <a href="/admin/" class="btn"><i class="icon-exit"></i> Log in</a>
   </div>
<? endif ?>
  </div>
