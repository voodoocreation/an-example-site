<section class="editor-pages" ng-app="ngPages" ng-controller="PagesCtrl" ng-cloak>
 <aside>
  <button type="button" ng-click="New()">Create new page</button>
  <ul class="nav">
   <li ng-repeat="page in pages"><a href="" ng-click="Edit(page.id)">{{page.title}}</a></li>
  </ul>
 </aside>

 <div class="form-editor">
  <form role="form">
   <div class="form-group">
    <label for="title">Title</label>
    <input id="title" name="title" type="text" ng-model="currentPage.title" required />
   </div>
   <div class="form-group">
    <label for="slug">Slug</label>
    <input id="slug" name="slug" type="text" ng-model="currentPage.slug" required />
   </div>
   <div class="form-group">
    <label for="class">Class</label>
    <input id="class" name="class" type="text" ng-model="currentPage.class" />
   </div>
   <div class="form-group">
    <label for="description">Description</label>
    <input id="description" name="description" type="text" ng-model="currentPage.description" required />
   </div>
   <div class="form-group">
    <label for="content">Content</label>
    <textarea id="content" name="content" ng-model="currentPage.content" required></textarea>
   </div>
   <button type="button" ng-click="Save()">Save</button>
  </form>
 </div>
</section>