  <div class="form-login">
   <form role="form" method="post" action="/admin/">
    <div class="form-group">
     <label for="username">Username</label>
     <input id="username" name="username" type="text" value="<?= isset($_POST["username"]) ? $_POST["username"] : null ?>" required />
    </div>
    <div class="form-group">
     <label for="password">Password</label>
     <input id="password" name="password" type="password" required />
    </div>
    <button type="submit">Log in</button>
   </form>
  </div>