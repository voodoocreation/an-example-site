<p>Welcome to quite possibly the quickest makeshift CMS ever made!</p>

<div class="site-featured">
 <div class="row">
  <div class="item">
   <a href="/admin/pages/">
    <div class="text">
     <h2>Pages</h2>
     <p>Edit existing pages and create new ones.</p>
    </div>
    <div class="icon-file"></div>
   </a>
  </div>

  <div class="item">
   <a href="/admin/users/">
    <div class="text">
     <h2>Users</h2>
     <p>Edit existing users and create new ones.</p>
    </div>
    <div class="icon-users"></div>
   </a>
  </div>
 </div>
</div>