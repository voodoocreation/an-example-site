    <nav class="breadcrumbs" itemprop="Breadcrumb">
     <p>
<? foreach ($crumbs as $path => $page): ?>
      <a href="<?= $path ?>"<? if (!empty($page["level"])): ?> rel="<?= $page["level"] ?>"<? else: ?> class="active"<? endif ?>><?= $page["title"] ?></a><? if ($path !== $_SERVER["REQUEST_URI"]): ?> &gt;<? endif ?>

<? endforeach ?>
     </p>
    </nav>